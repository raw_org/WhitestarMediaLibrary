/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.cmc.music.common.ID3ReadException;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.metadata.MusicMetadataSet;
import org.cmc.music.myid3.MyID3;



import org.vorbis.tageditor.OggFile;


/**
 *
 * @author Robert Worrall
 * This class is a wrapper class that uses the myID3 library to read Metadata from a mp3 file using the myID3 simple interface
 * It also reads ogg metadata using the jOggTagEditor library: http://joggtageditor.sourceforge.net/
 */
public class MetadataManipulator {

    private File currentFile;
    private MusicMetadata mp3MetaData;
    private String album;
    private Integer year;
    private int trackNumber;
    private String title;
    private String genre;
    private String artist;
    private List<String> comments;

    private void openMetaData(FileTypes fileTypes) {
        System.out.println("opening metadata");
        switch (fileTypes) {
            case mp3:
                openMYID3();
                break;
            case ogg:
                openOGG();
                break;
            default:
                album = "";
                year = 0;
                trackNumber = 0;
                title = "";
                genre = "unknown";
                artist = "";
                break;

        }
    }

    public void setFile(File mediaFile) {
        this.currentFile = mediaFile;
        //establish what file type being opened 
        String filename = mediaFile.getName();
        //System.out.println("Opening metadata for " + mediaFile.getAbsolutePath());
        String extension = filename.substring(filename.lastIndexOf(".") + 1);
        //System.out.println("extension is " + extension);

        //System.out.println("Extension file type is " + currentType);
        if (extension.equalsIgnoreCase(FileTypes.mp3.toString())) {
            //file is an MP3 use MYID3 to read the file
            currentType = FileTypes.mp3;

        } else {
            if (extension.equalsIgnoreCase(FileTypes.ogg.toString())) {
                //file is an ogg file use jOggTagEditor
                currentType = FileTypes.ogg;
                mp3MetaData = null;
            } else {
                currentType = FileTypes.unknown;
            }
        }
        //System.out.println("Extension file type is " + currentType);
        openMetaData(currentType);
    }

    @SuppressWarnings("unchecked")
	private void openMYID3() {
        if (currentFile != null) {
            try {
                MusicMetadataSet metadataSet = new MyID3().read(currentFile);
                
                mp3MetaData = (MusicMetadata) metadataSet.getSimplified();
                
               
                try {
                album = mp3MetaData.getAlbum();
                } catch (NullPointerException e){
                album = "Unknown";
                }
                
                try {
                year = mp3MetaData.getYear().intValue();
                } catch (NullPointerException e){
                year = 0;
                }
                try {
                trackNumber = mp3MetaData.getTrackCount().intValue();
                } catch (NullPointerException e){
                trackNumber = 0;
                }
                try {
                title = mp3MetaData.getSongTitle();
                } catch (NullPointerException e){
                title = "Unknown";
                }
                
                try {
                genre = mp3MetaData.getGenreName();
                } catch (NullPointerException e){
                genre = "Unknown";
                }
                
                try {
                artist = mp3MetaData.getArtist();
                } catch (NullPointerException e){
                artist = "Unknown";
                }
                try {
                comments = mp3MetaData.getComments();
                } catch (NullPointerException e){
                comments = new ArrayList<String>();
                }
                
                
                
                
                
            } catch (IOException ex) {
                Logger.getLogger(MetadataManipulator.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ID3ReadException ex) {
                Logger.getLogger(MetadataManipulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void openOGG() {
       // System.out.println("Opening ogg file" + currentFile);
        if (currentFile != null) {
            try {              
                OggFile ogg = new OggFile(currentFile);                               
                album = ogg.getComments().getProperty("ALBUM");
                if (album == null) {
                    album = "";
                }
                String yearString = ogg.getComments().getProperty("YEAR");
                if (yearString == null) {
                    year = 0;
                } else {
                    year = new Integer(yearString).intValue();
                }
                String trackString = ogg.getComments().getProperty("TRACKNUMBER");
                if (trackString == null) {
                    trackNumber = 0;
                } else {
                    trackNumber = new Integer(trackString).intValue();
                }
                title = ogg.getComments().getProperty("TITLE");
                if (title == null) {
                    title = "";
                }
                genre = ogg.getComments().getProperty("GENRE");
                if (genre == null) {
                    genre = "";
                }
                artist = ogg.getComments().getProperty("ARTIST");
                if (artist == null) {
                    artist = "";
                }
                /* System.out.println("remaining elements are");
                Enumeration<?> propertyNames = ogg.getComments().propertyNames();
                Enumeration<Object> elements = ogg.getComments().elements();
                while (propertyNames.hasMoreElements()) {
                System.out.println("Element available " + propertyNames.nextElement().toString());
                }
                System.out.println("remaining values are ");
                Object[] items = ogg.getComments().values().toArray();
                for (Object item : items) {
                System.out.println(item.toString());
                }*/


            } catch (IOException ex) {
                Logger.getLogger(MetadataManipulator.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error opening metadata on Ogg file " + ex);
            } catch (Exception ex) {
                Logger.getLogger(MetadataManipulator.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error opening metadata on Ogg file " + ex);
            }
        }



    }

    private enum FileTypes {

        mp3, ogg, unknown
    }
    private FileTypes currentType;

    public MetadataManipulator() {
    }

    public MetadataManipulator(File mediaFile) {
        setFile(mediaFile);
    }

    public String getAlbum() {
        return album;
    }

    public Integer getYear() {

        return year;
    }

    public int getTrackNumber() {
        return trackNumber;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getArtist() {
        return artist;
    }

    public List <String> getComments() {
        return comments;
    }
}
