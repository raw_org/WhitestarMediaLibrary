
/**
Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

/*
 * This class implements the WhitestarDBManagerInterface interface for a Music library using an embeded DB40 database.
 */
package Utilities;

import Enums.CategoryOptions;
import Enums.MediaType;
import Exception.MediaNotFoundException;
import WhitestarMedia.WhitestarMedia;
import Exception.UnsupportedMediaException;
import WhitestarMedia.WhitestarMediaComparator;
import WhitestarMedia.WhitestarMusic;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import java.util.Collection;
import java.util.List;


public class DB40MusicManager implements WhitestarDBManagerInterface {
    
    ObjectContainer currentDataBase;
    private EmbeddedConfiguration db4oConfig;


    
    
    @Override
    public boolean open() {
        db4oConfig = Db4oEmbedded.newConfiguration();
        //allow the older versions of the data base to be loaded.
        db4oConfig.common().allowVersionUpdates(true);
        //now open the database
        currentDataBase = Db4oEmbedded.openFile(db4oConfig, "whitestarMusicDataBase.wmd"); //wmd ie. whitstar media database
        System.out.println("Opened Database - databaseOpened ");
        return true;
    }

    @Override
    public boolean close() {
        boolean close = currentDataBase.close();
        System.out.println("Closing database - datebase closed " + close);
        return close;
    }

    

    @Override
    public WhitestarMedia getMedia(long id) {
        if(currentDataBase.query().execute().isEmpty()){
        return null;
        }else{
                ObjectSet<WhitestarMusic> result = currentDataBase.query(new Predicate<WhitestarMusic>() {

            /**
					 * 
					 */
					private static final long serialVersionUID = -1109000265362747602L;

			@Override
            public boolean match(WhitestarMusic media) {
                boolean aMatch = false;
                if (media.getId() == id) {
                    aMatch = true;

                } else {
                    aMatch = false;
                }
                return aMatch;
            }
        });
                return result.get(0);
        }
    }

    public List<WhitestarMusic> getListing(CategoryOptions Category){
    List<WhitestarMusic> resultSet = currentDataBase.query(new Predicate<WhitestarMusic>() {

            /**
		 * 
		 */
		private static final long serialVersionUID = -177687740114964758L;

			@Override
            public boolean match(WhitestarMusic media) {
                boolean aMatch = false;
                if (media.getCategories() == null) {
                    //see if subCategory is set if so then try to resolve it's Category
                    String subCat = media.getGenre();
                    if (subCat != null || subCat.equals("")) {
                        //no subCategory so a match is false.
                        aMatch = false;
                    } else {
                        // try to resolve the Category
                        media.setCategories(CategoryOptions.resolveCategory(subCat, MediaType.Music));
                        //now see if it matches with what we are looking for.
                        if (media.getCategories().contains(Category)) {
                            aMatch = true;
                        } else {
                            aMatch = false;
                        }
                    }
                } else {
                    //Category is set so check to see if it matches what we are looking for.
                    if (media.getCategories().contains(Category)) {
                        aMatch = true;

                    } else {
                        aMatch = false;
                    }
                }

                return aMatch;
            }
        });
    
    return resultSet;
    }
    
    
    
    @Override
    public <T extends WhitestarMedia> boolean store(T media) throws UnsupportedMediaException{
        if(media.getMediaType() == MediaType.Music){
        currentDataBase.store(media);
        return true;
        }
        else {
            throw new UnsupportedMediaException();           
        }
    }

    @Override
    public boolean IDinUse(long id) {
         //see if the number is in use
        ObjectSet<WhitestarMedia> result = currentDataBase.query(new Predicate<WhitestarMedia>() {

            /**
			 * 
			 */
			private static final long serialVersionUID = 3080370568464847683L;

			@Override
            public boolean match(WhitestarMedia media) {
                boolean aMatch = false;
                if (media.getId() == id) {
                    aMatch = true;

                } else {
                    aMatch = false;
                }
                return aMatch;
            }
        });
                if (result.isEmpty()) {
            return false;
        } else {
            return true;
        }
            
    }

    @Override
    public boolean deleteMedia(long id) throws MediaNotFoundException {

          ObjectSet<WhitestarMusic> result = currentDataBase.query(new Predicate<WhitestarMusic>() {

            /**
			 * 
			 */
			private static final long serialVersionUID = 5544173967373395344L;

			@Override
            public boolean match(WhitestarMusic media) {
                boolean aMatch = false;
                if (media.getId() == id) {
                    aMatch = true;

                } else {
                    aMatch = false;
                }
                return aMatch;
            }
        });
        if(result.isEmpty()){
        throw new MediaNotFoundException();
        }
        else{
        WhitestarMusic itemtobedeleted = result.get(0);
        currentDataBase.delete(itemtobedeleted);
        return true;
        }
    }

    @Override
    public <T extends WhitestarMedia> boolean updateMedia(T media) throws UnsupportedMediaException {
        return store(media);
    }

    @SuppressWarnings("unchecked")
	@Override
    public <T extends WhitestarMedia> List<WhitestarMusic> search(T media) throws MediaNotFoundException {
        List<WhitestarMusic> resultList;
        if(media.getMediaType() == MediaType.Music){
        WhitestarMusic predicate = (WhitestarMusic) media; 
        resultList = currentDataBase.query(new Predicate<WhitestarMusic>() {

            /**
			 * 
			 */
			private static final long serialVersionUID = 7985160341018907296L;

			@Override
            public boolean match(WhitestarMusic item) {
                boolean aMatch;
                
                 
                aMatch = WhitestarMediaComparator.similar(predicate,item);
                return aMatch;
            }
        });
        }
        else{
            throw new MediaNotFoundException(); 
        }
        return resultList;
    }
    
    
}
