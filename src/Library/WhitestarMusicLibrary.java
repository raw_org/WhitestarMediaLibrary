/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package Library;

import WhitestarMedia.WhitestarMusic;
import WhitestarMedia.WhitestarMedia;
import Utilities.MetadataManipulator;
import Enums.CategoryOptions;
import Enums.LibraryConfigurationKeys;
import Enums.MediaType;
import Exception.MediaNotFoundException;
import Exception.UnsupportedMediaException;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import Utilities.WhitestarDBManagerInterface;



/**
 *
 * @author Robert Worrall
 */
public class WhitestarMusicLibrary implements WhitestarMediaLibraryInterface {

    private WhitestarDBManagerInterface currentDataBase;


    public WhitestarMusicLibrary() {

    }

     @Override
    public void configure(Properties config) {
       
            try {
                currentDataBase = (WhitestarDBManagerInterface) Class.forName(config.getProperty(LibraryConfigurationKeys.DatabaseName.name())).newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                Logger.getLogger(WhitestarMusicLibrary.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
   
    /**
     * This method adds media to the database or updates the media into the database and returns it's ID
     */
    @Override
    public <t extends WhitestarMedia> long addMedia(t media) throws UnsupportedMediaException {
        //if the media has no ID number then assign one to it.
        if (media.getId() == 0) {
            media.setId(generateMediaID());
        }
        currentDataBase.store(media);
        return media.getId();
    }

    /**
     * This method adds all the recognised media in a given directory any files
     * or directories that can not be added are returned in a collection. It is a recursive method as it searches sub directories.
     */
    public Collection<String> addDirectory(File directory, Collection<String> allowedExtensions) {
        //create the collection to return the files/directories that could not be added.
        Collection<String> rejections = new ArrayList<String>();
        //see if the file is a directory
        if (directory.isDirectory()) {
            // if the file is a directory get the contents of the directory
            String[] directoryListing = directory.list();
            for (String x : directoryListing) {
                File aFile = new File(directory.getAbsolutePath() + x);
                //see if the file is a directory
                if (aFile.isDirectory()) {
                    //check the directory for further files
                    addDirectory(aFile, allowedExtensions);
                }
                // see if the file exists
                if (aFile.exists()) {
                    // the file exists now attempt to establish what type of file it is.
                    int position = x.indexOf(".");
                    String extension = x.substring(position);
                    if (allowedExtensions.contains(extension)) {
                        //if the file has a valid extentsion then add the file to the library
                        if (addFile(aFile) != 0) {
                            //file is added do nothing
                        } else {
                            rejections.add("Unable to add file " + aFile.getName());
                        }
                    } else {
                        rejections.add("File " + aFile.getName() + " does not have a valide file extenstion");
                    }
                } else {
                    rejections.add("File " + aFile.getName() + " does not exist");
                }
            }
        } else {
            //file is not a directory but is a file itself
            int position = directory.getName().indexOf(".");
            String extension = directory.getName().substring(position);
            //see if the extension is one that is allowed
            if (allowedExtensions.contains(extension)) {
                // if extension is allowed then add the file
                addFile(directory);
            } else {
                rejections.add("File " + directory.getName() + " does not have a valide file extenstion");
            }
        }
        return rejections;
    }

    /**
     * This method gets a given piece of media based on it's String ID
     */
    @Override
    public WhitestarMusic getMedia(final long id) throws MediaNotFoundException {

        WhitestarMusic media = (WhitestarMusic)currentDataBase.getMedia(id);

        return media;
    }

    /**
     * This method generates a unique id for the media to be stored in the database.
     */
    private long generateMediaID() {
        Random ran = new Random();
        long newID = ran.nextLong();
        if (IDinUse(newID) || newID == 0) {
            newID = generateMediaID();
        }
        return newID;
    }

    /**
     * This method checks to see if a id is already in use in the system.
     */
    private boolean IDinUse(final long newID) {
        return currentDataBase.IDinUse(newID);
    }

    /**
     * This method adds a file to the library returning its ID.
     */
    @Override
    public long addFile(File aFile) {
        long result = 0;
        //create a media file to place into the database.
        WhitestarMusic media = new WhitestarMusic();
        
            media.setFileLocation(aFile.toURI());
            MetadataManipulator meta = new MetadataManipulator(aFile);
            media.setAlbum(meta.getAlbum());
            media.setArtist(meta.getArtist());
            media.setCategories(CategoryOptions.resolveCategory(meta.getGenre(), MediaType.Music));
            media.setGenre(meta.getGenre());
            media.setTitle(meta.getTitle());
            media.setTrackNumber(meta.getTrackNumber());
            media.setYear(meta.getYear());
        try {
            result = addMedia(media);
        } catch (UnsupportedMediaException ex) {
            Logger.getLogger(WhitestarMusicLibrary.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    @Override
    public Collection<WhitestarMusic> getListing(CategoryOptions option) {
             return (Collection<WhitestarMusic>) currentDataBase.getListing(option);
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.Music;
    }

    
    

    @Override
    public boolean deleteMedia(long id) {
        
        try {
            currentDataBase.deleteMedia(id);
            return true;
        } catch (MediaNotFoundException ex) {
            Logger.getLogger(WhitestarMusicLibrary.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public <t extends WhitestarMedia> boolean updateMedia(t media) {
        try {
            currentDataBase.store(media);
            return true;
        } catch (UnsupportedMediaException ex) {
            Logger.getLogger(WhitestarMusicLibrary.class.getName()).log(Level.SEVERE, null, ex);
        return false;
        }
    }

   
}


