/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package Library;

import Enums.LibraryConfigurationKeys;
import Exception.LibraryCreationException;
import java.util.Properties;

/**
 * This uses a Factory patter for creating and configuring Whitestar Libaries based on property object that is handed to it. The properties file must have a Library property that corresponds to the class that is to be injected. 
 * @author Robert Worrall
 */
public class LibraryFactory {
    
    public static WhitestarMediaLibraryInterface createLibrary(Properties config) throws LibraryCreationException {
    
        try {
            WhitestarMediaLibraryInterface aLibrary;
            //pulls the class to use out of the config file
            String librarytobeInjected; // = config.getProperty("Library");
            librarytobeInjected = config.getProperty(LibraryConfigurationKeys.LibraryName.name());
            aLibrary = (WhitestarMediaLibraryInterface)Class.forName(librarytobeInjected).newInstance();
            aLibrary.configure(config);
            return aLibrary;
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
           // System.err.println("Error creating Library "+ config.getProperty("Library") +  ". Injection Error Class not Found.");
            throw new LibraryCreationException("Error creating Library "+ config.getProperty(LibraryConfigurationKeys.LibraryName.name()) +  ". Injection Error Class not Found.");
        }          
    }

    
}
