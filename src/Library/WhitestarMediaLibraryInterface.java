/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package Library;

import WhitestarMedia.WhitestarMusic;
import WhitestarMedia.WhitestarMedia;
import Enums.CategoryOptions;
import Enums.MediaType;
import Exception.LibraryCreationException;
import Exception.MediaNotFoundException;
import Exception.UnsupportedMediaException;
import java.io.File;
import java.util.Collection;
import java.util.Properties;

/**
 *
 * @author Robert Worrall
 */
public interface WhitestarMediaLibraryInterface {
    
    public void configure(Properties config)throws LibraryCreationException;
    
    /**
     *
     * @return returns the media type that the library deals with. 
     */
    public MediaType getMediaType();
    
    /**
     * 
     * @param <t> where t extends the WhitestarMedia class
     * @param media the media to be added to the library
     * @return returns the new id number of the item added
     * @throws Exception.UnsupportedMediaException when the media is of the incorrect type.
     */
    public <t extends WhitestarMedia> long  addMedia(t media) throws UnsupportedMediaException;
    
    public Collection<String> addDirectory(File directory, Collection<String> allowedExtensions);
 /**
     * This method adds a file to the library returning its ID.
     * @param aFile
     * @return the files id in the database. 
     */
    public long addFile(File aFile);
    /**
     * This method gets a given piece of media based on it's String ID
     * @param id
     * @throws Exception.MediaNotFoundException
     * @return returns an instance of WhitestarMedia
     */
    public WhitestarMedia getMedia(long id) throws MediaNotFoundException;
    
    /**
     * @param option the CatagotyOption which expresses which genre of media  to retrieve   
     * @return collection of whitestarmedia that match a given CatagoryOption
     */
    public Collection<? extends WhitestarMedia> getListing(final CategoryOptions option);   
    
    /**
     * deletes media from a given library
     * @param id the id number of the media item to be deleted 
     * @return a boolean if the delete was successful or not. 
     */
    public boolean deleteMedia(long id) throws MediaNotFoundException;
    
    /**
     * Updates media in the library.
     * 
     * @param <t> where t extends WhitestarMedia
     * @param media the media of type t where t extends WhitestarMedia
     * @return a boolean if the update was successful
     */
    public <t extends WhitestarMedia> boolean updateMedia(t media);
    
}
