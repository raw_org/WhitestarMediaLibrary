/*
 * The MIT License
 *
 * Copyright 2018 worralr.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Playlists;

import WhitestarMedia.WhitestarMedia;
import java.util.ArrayList;
import java.util.List;

/**
 * Interface for playlists that can be put in a given player that handles the
 * order in which a player will play a set of WhitestarMedia
 *
 * @author Robert Worrall
 */
public interface PlaylistInterface {

    public List<? extends WhitestarMedia> getFullTrackList();

    public WhitestarMedia getNextTrack();

    public void clearPlayList();

    public void setTrackList(List<WhitestarMedia> trackList);
/**
 * Get the mode the playlist is in i.e is going shuffle the tracks, play them in track number order etc. 
 */
    public PlayListModes getMode();

/**
 * Set the mode the playlist is in i.e is going shuffle the tracks, play them in track number order etc. 
 */
      public void setMode(PlayListModes mode);
    
}
