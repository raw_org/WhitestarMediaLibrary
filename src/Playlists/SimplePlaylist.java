/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package Playlists;

import WhitestarMedia.WhitestarMedia;
import WhitestarMedia.WhitestarMusic;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Robert Worrall
 *
 * This class implements a simple playlist functionality such as sorting,
 * returning the next track in the list
 */
public class SimplePlaylist implements PlaylistInterface {

    private List<WhitestarMedia> trackListing;
    private List<WhitestarMedia> playedList;
    private PlayListModes mode = PlayListModes.Sequential;

    private int positionInTrackListing = 0;

    public SimplePlaylist() {
        playedList = new ArrayList<WhitestarMedia>();

    }

    @Override
    public WhitestarMedia getNextTrack() {
        playedList.add(trackListing.get(positionInTrackListing));
        WhitestarMedia result = trackListing.get(positionInTrackListing);

        if (positionInTrackListing > trackListing.size()) {
            //System.out.println("not advancing position in tracking list");
            //do not increment the positionInTrackListing

        } else {
            // advance the trackList Position 
            positionInTrackListing++;
            //System.out.println("advancing position in tracking list to " + positionInTrackListing);
        }

        return result;
    }

    @Override
    public List<WhitestarMedia> getFullTrackList() {
        return new ArrayList<WhitestarMedia>(trackListing);
    }

    @Override
    public void setTrackList(List<WhitestarMedia> trackList) {
        this.trackListing = trackList;
        positionInTrackListing = 0;
    }

    @Override
    public void clearPlayList() {
        trackListing.clear();
    }

    @Override
    public PlayListModes getMode() {
        return mode;
    }

    /**
     * This sets the order of the playlist.
     *
     * @param mode is the enum describing the order of the list.
     */
    @Override
    public void setMode(PlayListModes mode) {
        this.mode = mode;
        if (mode == PlayListModes.Sequential) {
            trackListing = trackListing.stream().sorted().collect(Collectors.toList());
        } else {
            if (mode == PlayListModes.Shuffle) {
                Collections.shuffle(playedList);
            } else {
            }

        }
    }
}
