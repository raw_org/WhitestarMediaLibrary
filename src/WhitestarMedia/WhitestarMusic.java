/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package WhitestarMedia;


import Utilities.MetadataManipulator;
import Enums.CategoryOptions;
import Enums.MediaType;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Robert Worrall
 WhitestarMusic holds all the data regarding a piece of media stored in the library.
 */
public class WhitestarMusic extends WhitestarMedia{


     

    private String title;
    private String artist;
    private String album;
    private List <String> comments;
    private int trackNumber;
    private String genre;//genre taken from the actual file
    private Date lastPlayed;
    private Date dateAddedToLibrary;
    /*
    *This is for adjusting overly load or quiet tracks it is a value between 0.1 and 10.0 A value of 5 means no adjustment required. greater than 50 means an ajustment of up to 50% higher volume. A value less than 50 is an adjustment of upto 50% lower volume. To implement this correctly the player must have its player volume at 50%. This may only be acceptable when the system is connected to an external amplifier.   
    */
    private double level = 0.5; 

    
    
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

 

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }


   

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

   



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTrackNumber() {
        return trackNumber;
    }

    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }

    
/**
 * Method to fill empty media with data from a meta object
 */
public void fill(MetadataManipulator meta){

   album = meta.getAlbum();
   artist =  meta.getArtist();
   comments = meta.getComments();
   genre = meta.getGenre();
   title = meta.getTitle();
   trackNumber =  meta.getTrackNumber();
   setYear(meta.getYear());
   setCategories(CategoryOptions.resolveCategory(meta.getGenre(), MediaType.Music));

}

    @Override
    public MediaType getMediaType() {
        return MediaType.Music;
    }

    public Date getLastPlayed() {
        return lastPlayed;
    }

    public void setLastPlayed(Date lastPlayed) {
        this.lastPlayed = lastPlayed;
    }

    public Date getDateAddedToLibrary() {
        return dateAddedToLibrary;
    }

    public void setDateAddedToLibrary(Date addedToLibrary) {
        this.dateAddedToLibrary = addedToLibrary;
    }

    
    /**
     * This method returns the level the audio is adjusted by.  
     */
    public double getLevel() {
        return level;
    }
/**
 * This setter sets the level variable but ensures it is between 0.1 and 10; 
 */
    public void setLevel(float level) {
        if (level > 10){
        this.level = 10;
        }
        else {
        if (level < 0.1){
        this.level = 0.1;
        }
        else{
        this.level = level;
        }
        }
        
    }



  




    

}
