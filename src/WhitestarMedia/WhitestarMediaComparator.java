/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package WhitestarMedia;

import Enums.MediaType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Robert Worrall
 */
public class WhitestarMediaComparator implements Comparator<WhitestarMedia> {



    private static boolean compareText(String predicateText, String itemText) {

        return predicateText.equals(itemText) || itemText.matches(predicateText);

    }

    private static Boolean compareYear(int year, int year0) {
        Date dateA = new Date(year);
        Date dateb = new Date(year0);

        Calendar cal = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();
        cal.setTimeInMillis(year);
        cal2.setTimeInMillis(year0);
        int y = cal.get(Calendar.YEAR);
        int y1 = cal2.get(Calendar.YEAR);
        if (y == y1) {
            return true;
        } else {
            return false;
        }

    }

    /**
     *
     * @param t
     * @param t1
     * @return if t has a smaller id number than t1
     */
    @Override
    public int compare(WhitestarMedia t, WhitestarMedia t1) {
     
            if (t.getId() > t1.getId()) {
                return 1;
            } else {
                return -1;
            }
        } 
        
    

    /**
     *
     * @param predicate
     * @param musicTrack
     * @returns true if predicates attribues match either by regularExpression or
     * by exact value of those of the musicTrack
     */
    public static boolean similar(WhitestarMedia predicate, WhitestarMedia mediaTrack) {
        //first see if they are the same type
        if(predicate.getMediaType().equals(mediaTrack.getMediaType())){
        
            if(mediaTrack.getMediaType().equals(MediaType.Music)){
             //compare the attribues of the music and if there is a match on all attributes or if predicate item is set to null then jump. the results are held in a list    
           WhitestarMusic predicate2 = (WhitestarMusic) predicate;
           WhitestarMusic musicTrack = (WhitestarMusic) mediaTrack;
//check track title
        List<Boolean> results = new ArrayList<Boolean>();
        if (predicate2.getTitle() != null && musicTrack.getArtist() != null) {           
           results.add(compareText(predicate2.getTitle(), musicTrack.getTitle()));
        }
        //check artist
        if (predicate2.getArtist() != null && musicTrack.getArtist() != null) {
            results.add(compareText(predicate2.getArtist(), musicTrack.getArtist()));
        }
        //check album
        if (predicate2.getAlbum() != null && musicTrack.getAlbum() != null) {
            results.add(compareText(predicate2.getAlbum(), musicTrack.getAlbum()));
        }

        if (predicate2.getYear() != 0) {
            results.add(compareYear(predicate2.getYear(), musicTrack.getYear()));
        }
        
        //if all the results are true then return true;
       if(!results.isEmpty()){
        return !results.contains(false);}
       else {return false;}
            }
            else {
                return false;
            }
        }
        
        else{
        return false;
        }
        

       
    }

}
