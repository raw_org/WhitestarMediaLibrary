/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package WhitestarMedia;

import Enums.CategoryOptions;
import Enums.MediaType;
import java.net.URI;
import java.util.Date;
import java.util.List;

/**
 *
 * @author worralr
 */
public abstract class WhitestarMedia {
    private long id = 0;
    private URI fileLocation;
    private Date lastUpdated;
    private int year;
    private List<CategoryOptions> catagories;
    
    
    public List<CategoryOptions> getCategories() {
        return catagories;
    }

    public void setCategories(List<CategoryOptions> Category) {
        this.catagories = Category;
    }
    
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    
    public URI getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(URI fileLocation) {
        this.fileLocation = fileLocation;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
   
    public abstract MediaType getMediaType();
    
}
