/*
 Copyright (c) <2018> <Robert.worrall@r-a-w.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package Enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Robert Worrall 
 * This class is for resolving Category of media from it genre. This is a library can group large variations of the same genre
 * All definitions of music are taken from
 * wikipeadia.org.
 */
public enum CategoryOptions {

    ActionGame, Action_Adventure, Adventure, Role_Playing, Simulation, Strategy, Sports_Game, Party_Game, Massively_Multiplayer_Online, Logic,
    Blues, Country, Classical, Electronic, Folk, Jazz, Pop, Punk, Reggae, Rock, RapAndHiphop, Soul, Other;

    public static final String[] bluesList = {
        "blues",
        "acid blues",
        "blues-rock",
        "blues shouter",
        "boogie-woogie",
        "british blues",
        "canadian blues",
        "chicago blues",
        "classic female blues",
        "contemporary R&B",
        "country blues",
        "delta blues",
        "detroit blues",
        "electric blues",
        "gospel blues",
        "hokum blues",
        "jazz blues",
        "jump blues",
        "kansas City blues",
        "louisiana blues",
        "memphis blues",
        "piano blues",
        "piedmont blues",
        "punk blues",
        "rhythm and blues",
        "soul blues",
        "st. louis blues",
        "swamp blues",
        "texas blues",
        "west Coast blues"
    };
    public static final String[] countryList = {
        "country",
        "alternative country",
        "deathcountry",
        "gothic Americana",
        "hellbilly music",
        "outlaw country",
        "progressive country",
        "psydeco",
        "rap country",
        "red Dirt",
        "techno-Country",
        "texas Country",
        "americana",
        "australian country music",
        "bakersfield sound",
        "bluegrass",
        "heavy Metal Bluegrass",
        "neo-pop Bluegrass",
        "nu-grass",
        "bluegrass",
        "old-time bluegrass",
        "appalachian bluegrass",
        "progressive bluegrass",
        "reactionary bluegrass",
        "cajun",
        "christian country music",
        "classic country",
        "close harmony",
        "cowboy/Western music",
        "dansband music",
        "franco-country",
        "gulf and western",
        "honky tonk",
        "instrumental country",
        "lubbock Sound",
        "nashville Sound/",
        "countrypolitan",
        "neopop Country",
        "new country",
        "pop country",
        "cosmopolitan country",
        "sertanejo",
        "pop Country music",
        "truck-driving country",
        "western swing"
    };
    public static final String[] classicalList = {
        "classical",
        "afghan classical music ",
        "klasik",
        "andalusian classical music",
        "caliph of córdoba",
        "azerbaijani",
        "mugham",
        "bangladeshi classical music",
        "cambodian ceremonial music ",
        "pinpeat",
        "khmer empire chinese pop music",
        "gongche",
        "european classical music",
        "indian classical music",
        "gamelan",
        "persian classical music",
        "elamnite dynasties",
        "japanese court music",
        "gagaku",
        "korean court music",
        "lao classical music",
        "mandé art music",
        "griot",
        "ottoman classical music",
        "philippine art songs",
        "kundiman",
        "late Spanish Colonial Period Scottish Pibroch",
        "ceòl mór",
        "great highland bagpipe",
        "thai classical music",
        "piphat",
        "khmer E"
    };
    public static final String[] electronicList = {
        "electronic",
        "gamewave",
        "post-disco",
        "dance-pop",
        "boogie",
        "trance",
        "acid trance",
        "classic trance",
        "dream trance",
        "euro-trance",
        "hard trance",
        "hardstyle",
        "progressive trance",
        "psychedelic trance",
        "goa trance",
        "dark psy",
        "full on",
        "nitzhonot",
        "psyprog",
        "psybient",
        "psybreaks",
        "south african psytrance",
        "suomisaundi",
        "tech trance",
        "uplifting trance",
        "vocal trance",
        "nu rave",
        "hardcore breaks",
        "rave breaks",
        "jungle techno",
        "techno",
        "acid techno",
        "detroit techno",
        "free tekno",
        "ghettotech",
        "minimal",
        "new beat",
        "nortec",
        "schranz / hardtechno",
        "tech house",
        "tech trance",
        "techno-dnb",
        "techstep",
        "yorkshire techno"
    };
    public static final String[] folkList = {"folk",};
    public static final String[] jazzList = {
        "jazz",
        "Acid jazz",
        "Afro-jazz",
        "Asian American jazz",
        "Avant-garde jazz",
        "Bebop",
        "Bossa nova",
        "British dance band",
        "Calypso jazz Carribean",
        "Cape jazz",
        "Chamber jazz",
        "Contemporary jazz",
        "Continental Jazz",
        "Cool jazz",
        "Crossover jazz",
        "Cubop",
        "Dixieland",
        "Ethno jazz",
        "European free jazz",
        "Free funk",
        "Free improvisation",
        "Free jazz",
        "Gypsy jazz",
        "Hard bop",
        "Jazz blues",
        "Jazz-funk",
        "Jazz fusion",
        "Jazz rap",
        "Jazz rock",
        "Kansas City blues",
        "Kansas City jazz",
        "Latin",
        "Livetronica",
        "M-Base",
        "Mainstream jazz",
        "Modal jazz",
        "Modern Creative",
        "M-Base",
        "Neo-bop jazz",
        "Neo-swing",
        "Novelty ragtime",
        "Nu jazz",
        "Orchestral jazz",
        "Post-bop",
        "Punk jazz",
        "Ragtime",
        "Ska jazz",
        "Sci-Fi Jazz",
        "Shibuya-kei",
        "Ska jazz",
        "Smooth jazz",
        "Soul jazz",
        "Splab",
        "Stride jazz",
        "Straight-ahead jazz",
        "Swing",
        "Third stream",
        "Trad jazz",
        "Urban jazz",
        "Vocal jazz",
        "West Coast Gypsy jazz",
        "West Coast jazz"
    };
    public static final String[] punkList = {
        "punk",
        "anarcho punk",
        "taqwacore",
        "christian punk",
        "crust punk",
        "garage punk",
        "glam punk",
        "grunge",
        "hardcore punk",
        "horror punk",
        "noise rock",
        "oi!",
        "riot grrrl",
        "skate punk",
        "street punk",
        "trallpunk",
        "tone",
        "celtic punk",
        "chicano punk",
        "cowpunk",
        "dance-punk",
        "deathrock",
        "folk punk",
        "gypsy punk",
        "pop punk",
        "psychobilly",
        "punk blues",
        "punk metal",
        "ska punk",
        "street pop"
    };
    public static final String[] reggaeList = {
        "reggae",
        "early reggae",
        "skinhead reggae",
        "nyabhingi",
        "dub",
        "deejaying",
        "roots reggae",
        "dub poetry",
        "rockers style",
        "singjay",
        "sensi-beat",
        "sleng-teng",
        "dancehall",
        "lovers rock",
        "british lover's rock",
        "ragga",
        "raggamuffin",
        "reggae fusion",
        "reggaefusion",
        "rumble",
        "reggae singers",
        "reggae culture",
        "steppa"
    };
    public static final String[] rockList = {"rock",
        " 2 tone",
        "acid rock",
        "alternative dance",
        "alternative metal",
        "alternative rock",
        "anatolian rock",
        "arena rock",
        "art punk",
        "art rock",
        "avant-garde metal",
        "baroque pop",
        "beat",
        "bisrock",
        "black metal",
        "blackened death metal",
        "blues-rock",
        "brazilian rock",
        "britpop",
        "bubblegum pop",
        "c86",
        "canterbury sound",
        "cello rock",
        "celtic metal",
        "celtic rock",
        "chicano rock",
        "christcore",
        "christian rock",
        "christian metal",
        "coldwave",
        "comedy rock",
        "country rock",
        "crossover thrash",
        "crunkcore",
        "cuddlecore",
        "dance-rock",
        "dark cabaret",
        "darkwave",
        "death 'n' roll",
        "deathcore",
        "deathgrind",
        "death metal",
        "death rock",
        "deathcore",
        "doom metal",
        "dream pop",
        "drone metal",
        "dunedin sound",
        "electric folk",
        "electro punk",
        "electronic hardcore",
        "electronic rock",
        "emo",
        "experimental metal",
        "experimental rock",
        "folktronica",
        "folk rock",
        "folk metal",
        "folk punk",
        "freakbeat",
        "funk metal",
        "funk rock",
        "garage rock",
        "garage punk",
        "glam metal",
        "glam punk",
        "glam rock",
        "goregrind",
        "gothic metal",
        "gothic rock",
        "grebo",
        "grindcore",
        "groove metal",
        "group sounds",
        "grunge",
        "gypsy punk",
        "hatecore",
        "hard rock",
        "hardcore punk",
        "heartland rock",
        "heavy metal",
        "horror punk",
        "indie pop",
        "indie rock",
        "indorock",
        "industrial metal",
        "industrial rock",
        "instrumental rock",
        "j-ska",
        "jam rock",
        "jangle pop",
        "jersey shore sound",
        "krautrock",
        "lo-fi",
        "lovers rock",
        "madchester",
        "manguebeat",
        "manila sound",
        "mathcore",
        "math rock",
        "medieval metal",
        "melodic black metal",
        "melodic death metal",
        "melodic hardcore",
        "metalcore",
        "mod revival",
        "nardcore",
        "nazi punk",
        "neue deutsche welle",
        "neo-classical metal",
        "neo-folk",
        "neo-prog",
        "neo-psychedelia",
        "new prog",
        "new wave",
        "nintendocore",
        "noisecore",
        "noise pop",
        "noise rock",
        "no wave",
        "nu metal",
        "oi!",
        "ostrock",
        "pagan rock",
        "paisley underground",
        "pinoy rock",
        "pirate metal",
        "pop rock",
        "post-britpop",
        "post-grunge",
        "post-hardcore",
        "post-metal",
        "pornogrind",
        "post-rock",
        "power pop",
        "power metal",
        "power violence",
        "progressive metal",
        "progressive rock",
        "power pop",
        "psychedelic rock",
        "psych-folk",
        "psychobilly",
        "punk rock",
        "punta rock",
        "queercore",
        "raga rock",
        "rapcore",
        "rap metal",
        "rap rock",
        "riot grrrl",
        "rock and roll",
        "rockabilly",
        "rock noir",
        "rockoson",
        "sadcore",
        "samba-rock",
        "screamo",
        "shoegazing",
        "shock rock",
        "ska punk",
        "skate punk",
        "skate rock",
        "sludge metal",
        "soft rock",
        "southern rock",
        "space rock",
        "speed metal",
        "stoner metal",
        "stoner rock",
        "street punk",
        "sunshine pop",
        "sufi rock",
        "surf music",
        "surf rock",
        "swamp rock",
        "symphonic metal",
        "symphonic rock",
        "synthpop",
        "thrash metal",
        "thrashcore",
        "trip rock",
        "twee pop",
        "unblack metal",
        "viking metal",
        "viking rock",
        "visual kei",
        "wagnerian rock",
        "wizard rock",
        "zeuhl"
    };
    public static final String[] rapAndHiphopList = {
        "rapandhiphop",
        "Alternative hip hop",
        "Christian hip hop",
        "Comedy hip hop",
        "Conscious hip hop",
        "Country-rap",
        "Crunk",
        "Crunkcore",
        "Electro hop*",
        "Electro music*",
        "Freestyle music",
        "Freestyle rap",
        "Funk carioca",
        "G-Funk",
        "Gangsta rap",
        "Ghetto house*",
        "Ghettotech*",
        "Grime*",
        "Hardcore hip hop",
        "Hip hop soul***",
        "Hip house*",
        "House music*",
        "Horrorcore",
        "Hyphy",
        "Instrumental hip hop",
        "Jazz rap",
        "Lyrical hip hop",
        "Mafioso rap",
        "Nerdcore hip hop",
        "New jack swing",
        "Nu metal",
        "Political hip hop",
        "Post-gangsta",
        "Pop-rap**",
        "Ragga",
        "Reggaeton",
        "Rap opera",
        "Rap rock",
        "Rapcore",
        "Rap metal",
        "Snap music",
        "Turntablism"
    };
    public static final String[] popList = {"pop",};
    public static final String[] soulList = {"soul",
        "detroit soul",
        "motown",
        "deep soul",
        "southern soul",
        "memphis soul",
        "new orleans soul",
        "chicago soul",
        "philadelphia soul",
        "psychedelic soul",
        "blue-eyed soul",
        "british soul",
        "neo soul",
        "northern soul",
        "modern soul",
        "nu-jazz",
        "soulful electronica"
    };

    public static List<CategoryOptions> resolveCategory(String cat, MediaType type) {
        //conver the string to lower case so that it can compare it easier to those in the lists
        cat = cat.toLowerCase();
        //create a list to return
        List<CategoryOptions> returnList;
        // split to Music, Film, TV, Game, and run the correct resolve Category method 
        switch (type) {
            case Film:
                returnList = resolveFilmCategory(cat);
                break;
            case Game:
                returnList = resolveGameCategory(cat);
                break;
            case Music:
                returnList = resolveMusicCategory(cat);
                break;
            case TV:
                returnList = resolveTVCategory(cat);
                break;
            default:
                returnList = new ArrayList<CategoryOptions>();
        }

        return returnList;

    }

    private static boolean isBlues(String cat) {
        return Arrays.asList(bluesList).contains(cat);

    }

    private static boolean isCountry(String cat) {
        return Arrays.asList(countryList).contains(cat);
    }

    private static boolean isClassical(String cat) {
        return Arrays.asList(classicalList).contains(cat);
    }

    private static boolean isElectronic(String cat) {
        return Arrays.asList(electronicList).contains(cat);
    }

    private static boolean isFolk(String cat) {
        return Arrays.asList(folkList).contains(cat);
    }

    private static boolean isJazz(String cat) {
        return Arrays.asList(jazzList).contains(cat);
    }

    private static boolean isReggae(String cat) {
        return Arrays.asList(reggaeList).contains(cat);
    }

    private static boolean isPunk(String cat) {
        return Arrays.asList(punkList).contains(cat);
    }

    private static boolean isRock(String cat) {
        return Arrays.asList(rockList).contains(cat);
    }

    private static boolean isRapAndHiphop(String cat) {
        return Arrays.asList(rapAndHiphopList).contains(cat);
    }

    private static boolean isPop(String cat) {
        return Arrays.asList(popList).contains(cat);
    }

    private static boolean isSoul(String cat) {
        return Arrays.asList(soulList).contains(cat);
    }

//Gameing classifications
    public static final String[] ActionList = {
        "Platform games",
        "Shooter games",
        "Fighting games",
        "Beat 'em up games",
        "Stealth game",
        "Survival games",
        "Rhythm games"

    };

    public static final String[] Action_AdventureList = {
        "Survival horror",
        "2.2 Metroidvania"

    };

    public static final String[] AdventureList = {
        "Text adventures",
        "Graphic adventures",
        "Visual novels",
        "Interactive movie",
        "Real-time 3D adventures"

    };

    public static final String[] Role_playingList = {
        "Action RPG",
        "MMORPG",
        "Roguelikes",
        "Tactical RPG",
        "Sandbox RPG",
        "First-person party-based RPG",
        "Cultural differences",
        "Choices",
        "Fantasy"

    };

    public static final String[] SimulationList = {
        "Construction and management simulation",
        "Life simulation",
        "Vehicle simulation"

    };

    public static final String[] StrategyList = {
        "4X game",
        "Artillery game",
        "Real-time strategy (RTS)",
        "Real-time tactics (RTT)",
        "Multiplayer online battle arena (MOBA)",
        "Tower defense",
        "Turn-based strategy (TBS)",
        "Turn-based tactics (TBT)",
        "Wargame",
        "Grand strategy wargame"

    };

    public static final String[] Sports_GameList = {
        "Racing",
        "Sports game",
        "Competitive",
        "Sports-based fighting"

    };

    private static List<CategoryOptions> resolveFilmCategory(String cat) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static List<CategoryOptions> resolveGameCategory(String cat) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static List<CategoryOptions> resolveMusicCategory(String cat) {
        List<CategoryOptions> returnList = new ArrayList<CategoryOptions>();

        if (isBlues(cat)) {
            returnList.add(Blues);
        }
        if (isCountry(cat)) {
            returnList.add(Country);
        }
        if (isClassical(cat)) {
            returnList.add(Classical);
        }
        if (isElectronic(cat)) {
            returnList.add(Electronic);
        }
        if (isFolk(cat)) {
            returnList.add(Folk);
        }
        if (isJazz(cat)) {
            returnList.add(Jazz);
        }
        if (isPunk(cat)) {
            returnList.add(Punk);
        }
        if (isReggae(cat)) {
            returnList.add(Reggae);
        }
        if (isRock(cat)) {
            returnList.add(Rock);
        }
        if (isRapAndHiphop(cat)) {
            returnList.add(RapAndHiphop);
        }
        if (isPop(cat)) {
            returnList.add(Pop);
        }
        if (isSoul(cat)) {
            returnList.add(Soul);
        }
        if (returnList.isEmpty()) {
            returnList.add(Other);
        }
        return returnList;

    }

    private static List<CategoryOptions> resolveTVCategory(String cat) {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
