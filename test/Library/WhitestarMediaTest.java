/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Library;

import WhitestarMedia.WhitestarMusic;
import java.util.ArrayList;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Worrall
 */
public class WhitestarMediaTest {

    public WhitestarMediaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of getAlbum method, of class WhitestarMusic.
     */
    @Test
    public void testGetAlbum() {
        System.out.println("getAlbum");
        WhitestarMusic instance = new WhitestarMusic();
        String albumName = "Groceries and Rock & Roll. Oh Yea!!!";
        instance.setAlbum(albumName);
        String expResult = albumName;
        String result = instance.getAlbum();
        assertThat(expResult, equalTo(result));
        
    }

    /**
     * Test of setAlbum method, of class WhitestarMusic.
     */
    @Test
    public void testSetAlbum() {
        System.out.println("setAlbum");
        String album = "test";
        WhitestarMusic instance = new WhitestarMusic();
        instance.setAlbum(album);
        String expResult = "test";
        String result = instance.getAlbum();
        assertTrue(result.equals(expResult));
        
    }

    /**
     * Test of getArtist method, of class WhitestarMusic.
     */
    @Test
    public void testGetArtist() {
        System.out.println("getArtist");
        WhitestarMusic instance = new WhitestarMusic();
        String artist = "The Rolling Stones";
        instance.setArtist(artist);
        String expResult = artist;
        String result = instance.getArtist();
        assertEquals(expResult, result);
    }

    /**
     * Test of setArtist method, of class WhitestarMusic.
     */
    @Test
    public void testSetArtist() {
        System.out.println("setArtist");
        String artist = "test";
        WhitestarMusic instance = new WhitestarMusic();
        instance.setArtist(artist);
        String expResult = "test";
        String result = instance.getArtist();
        assertEquals(expResult,result);
    }

    /**
     * Test of getComment method, of class WhitestarMusic.
     */
    @Test
    public void testGetComment() {
        System.out.println("getComment");
        WhitestarMusic instance = new WhitestarMusic();
        List <String> comments = new ArrayList<>();
        comments.add("Come and see us at our corner shop on stella street. Oh Yea...");
        instance.setComments(comments);
        String expResult = "Come and see us at our corner shop on stella street. Oh Yea...";
        String result = instance.getComments().get(0);
        assertEquals(expResult, result);

    }

    /**
     * Test of setComment method, of class WhitestarMusic.
     */
    @Test
    public void testSetComment() {
        System.out.println("setComment");
        String comment = "test";
        WhitestarMusic instance = new WhitestarMusic();
        List <String> comments = new ArrayList<>();
        comments.add(comment);
        instance.setComments(comments);
        String expResult = "test";
        String result = instance.getComments().get(0);
        assertTrue(expResult.equals(result));

    }

    /**
     * Test of getFileLocation method, of class WhitestarMusic.
     */
    @Test
    public void testGetFileLocation() {
        System.out.println("getFileLocation");
        WhitestarMusic instance = new WhitestarMusic();
        URI expResult = null;
        URI result = instance.getFileLocation();
        assertTrue(expResult == result);
    }

    /**
     * Test of setFileLocation method, of class WhitestarMusic.
     */
    @Test
    public void testSetFileLocation() {
        try {
            System.out.println("setFileLocation");
            URI fileLocation = new URI("www.yahoo.com");
            WhitestarMusic instance = new WhitestarMusic();
            instance.setFileLocation(fileLocation);
            URI expResult = new URI("www.yahoo.com");
            URI result = instance.getFileLocation();
            assertEquals(result, expResult);
            
        } catch (URISyntaxException ex) {
            Logger.getLogger(WhitestarMediaTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("URI in test failed.");
        }
    }

    /**
     * Test of getGenre method, of class WhitestarMusic.
     */
    @Test
    public void testGetGenre() {
        System.out.println("getGenre");
        WhitestarMusic instance = new WhitestarMusic();
        instance.setGenre("Rock");
        String expResult = "Rock";
        String result = instance.getGenre();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of setGenre method, of class WhitestarMusic.
     */
    @Test
    public void testSetGenre() {
        System.out.println("setGenre");
        String genre = "test";
        WhitestarMusic instance = new WhitestarMusic();
        instance.setGenre(genre);
        String expResult = "test";
        String result = instance.getGenre();
        assertTrue(expResult.equals(result));
        
    }

    /**
     * Test of getId method, of class WhitestarMusic.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        WhitestarMusic instance = new WhitestarMusic();
        long expResult = 0L;
        long result = instance.getId();
        assertEquals(expResult, result);

    }

    /**
     * Test of setId method, of class WhitestarMusic.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        long id = 0L;
        WhitestarMusic instance = new WhitestarMusic();
        instance.setId(id);
        long expResult = 0L;
        long result = instance.getId();
        assertEquals(expResult, result);

    }

    /**
     * Test of getLastUpdated method, of class WhitestarMusic.
     */
    @Test
    public void testGetLastUpdated() {
        System.out.println("getLastUpdated");
        WhitestarMusic instance = new WhitestarMusic();
        Date expResult = null;
        Date result = instance.getLastUpdated();
        assertEquals(expResult, result);

    }

    /**
     * Test of setLastUpdated method, of class WhitestarMusic.
     */
    @Test
    public void testSetLastUpdated() {
        System.out.println("setLastUpdated");
        Date lastUpdated = new Date();
        WhitestarMusic instance = new WhitestarMusic();
        instance.setLastUpdated(lastUpdated);
        Date expResult =lastUpdated;
        Date result = instance.getLastUpdated();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitle method, of class WhitestarMusic.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        WhitestarMusic instance = new WhitestarMusic();
        String title = "Cash and Carry, Where's Barry!";
        String expResult = title;
        instance.setTitle(title);
        String result = instance.getTitle();
        assertEquals(expResult, result);
     }

    /**
     * Test of setTitle method, of class WhitestarMusic.
     */
    @Test
    public void testSetTitle() {
        System.out.println("setTitle");
        String title = "test";
        WhitestarMusic instance = new WhitestarMusic();
        instance.setTitle(title);
        String expResult = "test";
        String result = instance.getTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTrackNumber method, of class WhitestarMusic.
     */
    @Test
    public void testGetTrackNumber() {
        System.out.println("getTrackNumber");
        WhitestarMusic instance = new WhitestarMusic();
        int expResult = 0;
        int result = instance.getTrackNumber();
        assertEquals(expResult, result);

    }

    /**
     * Test of setTrackNumber method, of class WhitestarMusic.
     */
    @Test
    public void testSetTrackNumber() {
        System.out.println("setTrackNumber");
        int trackNumber = 1;
        WhitestarMusic instance = new WhitestarMusic();
        instance.setTrackNumber(trackNumber);
        int expResult = 1;
        int result = instance.getTrackNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getYear method, of class WhitestarMusic.
     */
    @Test
    public void testGetYear() {
        System.out.println("getYear");
        WhitestarMusic instance = new WhitestarMusic();
        int expResult = 0;
        int result = instance.getYear();
        assertEquals(expResult, result);

    }

    /**
     * Test of setYear method, of class WhitestarMusic.
     */
    @Test
    public void testSetYear() {
        System.out.println("setYear");
        int year = 1976;
        WhitestarMusic instance = new WhitestarMusic();
        instance.setYear(year);
        int expResult = 1976;
        int result = instance.getYear();
        assertEquals(expResult, result);
    }

}