/*
 * The MIT License
 *
 * Copyright 2018 worralr.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Library;

import Enums.LibraryConfigurationKeys;
import Exception.LibraryCreationException;
import java.util.Properties;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *This is to test the factory creation of libraries. 
 * @author Robert Worrall
 */
public class LibraryFactoryTest {
    
    private Properties configForMusicLibrary;
    
    public LibraryFactoryTest() {
        configForMusicLibrary = new Properties();
        configForMusicLibrary.setProperty(LibraryConfigurationKeys.LibraryName.name(), "Library.WhitestarMusicLibrary");
        configForMusicLibrary.setProperty(LibraryConfigurationKeys.DatabaseName.name(), "Utilities.DB40MusicManager");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createLibrary method which creates a Music Library.
     */
    @Test
    public void testCreateMusicLibrary() throws Exception {
        System.out.println("Create Music Library");
        WhitestarMediaLibraryInterface expResult = new WhitestarMusicLibrary();
        expResult.configure(configForMusicLibrary);  
        WhitestarMediaLibraryInterface result = LibraryFactory.createLibrary(configForMusicLibrary);
        assertThat(expResult.getClass(), equalTo(result.getClass()));
    }

    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    /**
     * Test for exception thrown when setting up injection class.
     */
    @Test 
            public void throwsExceptionWhenIncorrectLibraryClassInjected() throws LibraryCreationException{
                System.out.println("Test erronius injection class");
                thrown.expect(LibraryCreationException.class); 
                thrown.expectMessage("Error creating Library Library.BurpLibrary. Injection Error Class not Found.");
            Properties defectiveConfig = new Properties();
            defectiveConfig.setProperty(LibraryConfigurationKeys.LibraryName.name(), "Library.BurpLibrary");
            //throw the exception
            LibraryFactory.createLibrary(defectiveConfig);
            }
    
}
