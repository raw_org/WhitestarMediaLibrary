/*
 * The MIT License
 *
 * Copyright 2018 worralr.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Library;

import Enums.LibraryConfigurationKeys;
import Enums.MediaType;
import Exception.NoLibraryFoundException;
import java.util.List;
import java.util.Properties;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Robert Worrall
 */
public class WhitestarLibraryTest {
   
    private WhitestarLibrary instance;
    private Properties configForMusicLibrary;
    
    
    /**
     * set up a library and an instance 
     */
    public WhitestarLibraryTest() {
    
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    /**
     * Test of getLibrary method, of class WhitestarLibrary.
     */
    @Test
    public void testGetLibrary() throws Exception {
        //first create the libary to be added
        instance = new WhitestarLibrary();
        configForMusicLibrary = new Properties();
        configForMusicLibrary.setProperty(LibraryConfigurationKeys.LibraryName.name(), "Library.WhitestarMusicLibrary");
        configForMusicLibrary.setProperty(LibraryConfigurationKeys.DatabaseName.name(), "Utilities.DB40MusicManager");   
        instance.createLibrary(configForMusicLibrary);
        // create a second instance so that there are 2 music libraryies to be retrieved
        instance.createLibrary(configForMusicLibrary);
        //now see if a libaries are returned.
        System.out.println("getLibrary");
        MediaType type = MediaType.Music;
        
        
        List<WhitestarMediaLibraryInterface> result = instance.getLibrary(type);
        //check something has been retrieved.
        assertThat(result.isEmpty(), equalTo(false));
        //check there are 2 in the list
        assertThat(result.size(), equalTo(2));
        //check that the results are music libraries. 
        result.forEach(cnsmr -> assertThat(cnsmr.getMediaType(), equalTo(MediaType.Music)));         
    }
    
    /**
     * Test an exception when an incorrect configuration file has been fed to the createLibrary Method
     */
    @Test
    public void throwsLibraryNotFoundException() throws NoLibraryFoundException{
    instance = new WhitestarLibrary();
    thrown.expect(NoLibraryFoundException.class);
    thrown.expectMessage("No Librarys found for type: " + MediaType.Music);
    // throw the exception.
        instance.getLibrary(MediaType.Music);
    }
    
    

    /**
     * Test of createLibrary method, of class WhitestarLibrary.
     */
    @Test
    public void testCreateLibrary() throws Exception {
        
        System.out.println("createLibrary");
                instance = new WhitestarLibrary();
        configForMusicLibrary = new Properties();
        configForMusicLibrary.setProperty(LibraryConfigurationKeys.LibraryName.name(), "Library.WhitestarMusicLibrary");
        configForMusicLibrary.setProperty(LibraryConfigurationKeys.DatabaseName.name(), "Utilities.DB40MusicManager");   
        instance.createLibrary(configForMusicLibrary);
    }
    
}
