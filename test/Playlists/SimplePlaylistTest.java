/*
 * The MIT License
 *
 * Copyright 2018 Robert.worrall@r-a-w.org.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Playlists;

import WhitestarMedia.WhitestarMedia;
import WhitestarMedia.WhitestarMusic;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Worrall
 */
public class SimplePlaylistTest {

    WhitestarMusic mediaitem1;
    WhitestarMusic mediaitem2;
    WhitestarMusic mediaitem3;
    WhitestarMusic mediaitem4;
    List<WhitestarMedia> tracks;

    public SimplePlaylistTest() {
        mediaitem1 = new WhitestarMusic();
        mediaitem1.setArtist("The Rolling Stones");
        mediaitem1.setTitle("Cash and Carry, Wheres Barry");
        mediaitem1.setAlbum("Groceries and Rock... Oh Yeah...");
        mediaitem1.setTrackNumber(1);
        mediaitem2 = new WhitestarMusic();
        mediaitem2.setArtist("The Rolling Stones");
        mediaitem2.setTitle("Keith! Stop drinking the stock man.");
        mediaitem2.setAlbum("Groceries and Rock... Oh Yeah...");
        mediaitem2.setTrackNumber(2);
        mediaitem3 = new WhitestarMusic();
        mediaitem3.setArtist("The Rolling Stones");
        mediaitem3.setTitle("Have you seen the date of these tinned carrots");
        mediaitem3.setAlbum("Groceries and Rock... Oh Yeah...");
        mediaitem3.setTrackNumber(3);
        mediaitem4 = new WhitestarMusic();
        mediaitem4.setArtist("The Rolling Stones");
        mediaitem4.setTitle("Cornershop Rock");
        mediaitem4.setAlbum("Groceries and Rock... Oh Yeah...");
        mediaitem4.setTrackNumber(4);
        tracks = new ArrayList<WhitestarMedia>();
        tracks.add(mediaitem1);
        tracks.add(mediaitem2);
        tracks.add(mediaitem3);
        tracks.add(mediaitem4);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNextTrack method, of class SimplePlaylist.
     */
    @Test
    public void testGetNextTrack() {
        System.out.println("getNextTrack");
        SimplePlaylist instance = new SimplePlaylist();
        //WhitestarMedia expectedOrder[] = new WhitestarMedia[]{mediaitem1,mediaitem2, mediaitem3, mediaitem4};
        ArrayList<WhitestarMedia> trackList = new ArrayList<WhitestarMedia>();
        trackList.add(mediaitem1);
        trackList.add(mediaitem2);
        trackList.add(mediaitem3);
        trackList.add(mediaitem4);
        instance.setTrackList(trackList);
        WhitestarMedia expResult;        
        WhitestarMedia result;
        WhitestarMusic result2;
        //start testing the advancement of tracks.
        result = instance.getNextTrack();
        expResult = mediaitem1;
        //System.out.println("Mediaitem1 expResult: " + mediaitem1.getTitle());
        result2 = (WhitestarMusic) result;
        //System.out.println("result: " + result2.getTitle());
        assertThat(expResult, equalTo(result));
                result = instance.getNextTrack();
        expResult = mediaitem2;
       // System.out.println("Mediaitem1 expResult: " + mediaitem2.getTitle());
        result2 = (WhitestarMusic) result;
        //System.out.println("result: " + result2.getTitle());
        assertThat(expResult, equalTo(result));
                result = instance.getNextTrack();
        expResult = mediaitem3;
       // System.out.println("Mediaitem1 expResult: " + mediaitem3.getTitle());
        result2 = (WhitestarMusic) result;
      //  System.out.println("result: " + result2.getTitle());
        assertThat(expResult, equalTo(result));
                result = instance.getNextTrack();
        expResult = mediaitem4;
       // System.out.println("Mediaitem1 expResult: " + mediaitem4.getTitle());
        result2 = (WhitestarMusic) result;
      //  System.out.println("result: " + result2.getTitle());
        assertThat(expResult, equalTo(result));
        //try to get the next track it should be the last track in the list
         expResult = mediaitem4;
       // System.out.println("Mediaitem1 expResult: " + mediaitem4.getTitle());
        result2 = (WhitestarMusic) result;
      //  System.out.println("result: " + result2.getTitle());
        assertThat(expResult, equalTo(result));
    }

    /**
     * Test of getFullTrackList method, of class SimplePlaylist.
     */
    @Test
    public void testGetFullTrackList() {
        System.out.println("getFullTrackList");
        
        SimplePlaylist instance = new SimplePlaylist();
        instance.setTrackList(tracks);
        List<WhitestarMedia> expResult = tracks;
        List<WhitestarMedia> result = instance.getFullTrackList();
        assertThat(expResult, equalTo(result));
    }

    
    /**
     * Test of clearPlayList method, of class SimplePlaylist.
     */
    @Test
    public void testClearPlayList() {
        System.out.println("clearPlayList");
        SimplePlaylist instance = new SimplePlaylist();
        instance.setTrackList(tracks);
        instance.clearPlayList();
        assertThat(instance.getFullTrackList().isEmpty(), equalTo(true));
    }

    

}
