/*
 * The MIT License
 *
 * Copyright 2018 Robert.Worrall.r-a-w.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Utilities;

import Enums.CategoryOptions;
import Exception.UnsupportedMediaException;
import WhitestarMedia.WhiteStarMedia_Game;
import WhitestarMedia.WhitestarMedia;
import WhitestarMedia.WhitestarMusic;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.core.IsCollectionContaining;
import org.hamcrest.core.IsNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Worrall
 */
public class DB40MusicManagerTest {
    
    WhitestarMusic media;
    volatile DB40MusicManager database = new DB40MusicManager();
    
    public DB40MusicManagerTest() {
        media = new WhitestarMusic();
        media.setId(1);
        media.setArtist("The Rolling Stones");
        media.setTitle("Cash and Carry, Where's Barry!");
        
        }
    
    @BeforeClass
    public static void setUpClass() {
        File aFile = new File("whitestarMusicDataBase.wmd");
        if(aFile.exists()){
            //delete the file
            aFile.delete();
        }
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        
        }

    /**
     * Test of open method, of class DB40MusicManager.
     */
    @Test
    public void testOpen() {
        System.out.println("open");
        
        boolean expResult = true;
        boolean result = database.open();
        
        assertEquals(expResult, result);
        database.close();
    }

    /**
     * Test of close method, of class DB40MusicManager.
     */
    @Test
    public void testClose() {
        System.out.println("close");
        database.open();
        boolean expResult = true;
        boolean result = database.close();
        assertEquals(expResult, result);

    }

    /**
     * Test of store method, of class DB40MusicManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testStore() throws Exception {
        System.out.println("store");
        database.open();
        database.store(media);
        WhitestarMedia expResult = media;
        WhitestarMedia result = database.getMedia(1);
        database.deleteMedia(1);
        
        assertThat(expResult.getId(), equalTo(result.getId()));
        database.close();
    }
    
    
    /**
     * Test store method, of class DB40MusicManager so it throws an Unsupported Media Exception
     * @throws java.lang.Exception
     */
    @Test (expected = UnsupportedMediaException.class)
        public void testStoreException() throws Exception {
        System.out.println("store");
        database.open();
        try{database.store(new WhiteStarMedia_Game());}
        catch(UnsupportedMediaException e){ 
            database.close();
            throw new UnsupportedMediaException();
        }
       
    }
    
    
    /**
     * Test of getMedia method, of class DB40MusicManager.
     * @throws Exception.UnsupportedMediaException
     */
    @Test
    public void testGetMedia() throws UnsupportedMediaException {
        System.out.println("getMedia");
        long id = 1;
        database.open();
        database.store(media);
        WhitestarMedia expResult = media;
        WhitestarMedia result = database.getMedia(id);
        database.deleteMedia(id);
        assertThat(expResult, equalTo(result));
        database.close();
    }

    /**
     * Test of getListing method, of class DB40MusicManager.
     * @throws Exception.UnsupportedMediaException
     */
    @Test
    public void testGetListing() throws UnsupportedMediaException {
        System.out.println("getListing");
        CategoryOptions Category = CategoryOptions.Jazz;
        List<CategoryOptions> catagories = new ArrayList<>();
        catagories.add(CategoryOptions.Jazz);
        catagories.add(CategoryOptions.Pop);
        media.setCategories(catagories);
        database.open();
        database.store(media);
        List<WhitestarMusic> result = database.getListing(Category);
        
        assertThat(result.get(0).getArtist(), equalTo(media.getArtist()));
        database.deleteMedia(1);
        database.close();
    }

    

    /**
     * Test of IDinUse method, of class DB40MusicManager.
     * @throws Exception.UnsupportedMediaException
     */
    @Test
    public void testIDinUse() throws UnsupportedMediaException {
        System.out.println("IDinUse");  
        database.open();
        database.store(media);
        boolean expResult = true;
        boolean result = database.IDinUse(media.getId());
        assertEquals(expResult, result);
        database.deleteMedia(1);
        database.close();
        }

    /**
     * Test of deleteMedia method, of class DB40MusicManager.
     * @throws Exception.UnsupportedMediaException
     */
    @Test
    public void testDeleteMedia() throws UnsupportedMediaException {
        System.out.println("deleteMedia");
        long id = media.getId();
        database.open();
        database.store(media);
        boolean expResult = true;
        boolean result = database.deleteMedia(id);
        assertEquals(expResult, result);
        //make sure the media is removed from the library result should be null
        WhitestarMedia media1 = database.getMedia(id);
        assertThat(media1, IsNull.nullValue());
        
        database.close();

    }

    /**
     * Test of updateMedia method, of class DB40MusicManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateMedia() throws Exception {
        System.out.println("updateMedia");
        CategoryOptions CategoryToFind = CategoryOptions.Blues;
        List<CategoryOptions> catagories = new ArrayList<>();
        catagories.add(CategoryOptions.Jazz);
        catagories.add(CategoryOptions.Pop);
        media.setCategories(catagories);
        database.open();
        database.store(media);
        catagories.add(CategoryOptions.Blues);
        media.setCategories(catagories);
        database.updateMedia(media);
        List<WhitestarMusic> result = database.getListing(CategoryToFind);
        assertThat(result, IsCollectionContaining.hasItem(media));
        database.deleteMedia(1);
        database.close();
    }

    /**
     * Test of search method, of class DB40MusicManager.
     * @throws Exception.UnsupportedMediaException
     */
    @Test
    public void testSearch() throws UnsupportedMediaException {
        System.out.println("search");
        database.open();
        database.store(media);
        //create predicate to find item with said title.
        WhitestarMusic predicate = new WhitestarMusic();
        predicate.setArtist("The Rolling Stones");
        //List<WhitestarMusic> expResult = null;
        List<WhitestarMusic> result = database.search(predicate);
        System.out.println("result as found " + result.size() + " results");
        assertThat(result, IsCollectionContaining.hasItem(media));
        database.deleteMedia(1);
        database.close();
    }
    
}
