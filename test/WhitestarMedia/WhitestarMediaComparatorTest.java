/*
 * The MIT License
 *
 * Copyright 2018 robert.worrall@r-a-w.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package WhitestarMedia;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Worrall
 */
public class WhitestarMediaComparatorTest {

    WhitestarMedia media1;
    WhitestarMedia media2;
    WhitestarMedia media3;
    WhitestarMedia media4;

    public WhitestarMediaComparatorTest() {
        media1 = new WhitestarMusic();
        media1.setId(1);
        media2 = new WhiteStarMedia_Game();
        media2.setId(2);
        media3 = new WhitestarMusic();
        media3.setId(3);
        media4 = new WhiteStarMedia_Game();
        media4.setId(4);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class WhitestarMediaComparator between two different types. this should
     * return false.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        WhitestarMediaComparator instance = new WhitestarMediaComparator();
        int expResult = -1;
        int result = instance.compare(media1, media2);
        assertEquals(expResult, result);
    }

        /**
     * Test of compare method, of class WhitestarMediaComparator between two different types. this should
     * return 1.
     */
    @Test
    public void testCompare2() {
        System.out.println("compare");
        WhitestarMediaComparator instance = new WhitestarMediaComparator();
        int expResult = 1;
        int result = instance.compare(media4, media3);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of similarMusic method, of class WhitestarMediaComparator.
     */
    @Test
    public void testSimilarMusic() {
        System.out.println("similar");
        WhitestarMusic predicate = new WhitestarMusic();
        WhitestarMusic musicTrack = new WhitestarMusic();
        predicate.setArtist("The Rolling Stones");
        musicTrack.setArtist("The Rolling Stones");
        boolean expResult = true;
        boolean result = WhitestarMediaComparator.similar(predicate, musicTrack);
        assertEquals(expResult, result);
    }

        /**
     * Test of similarMusic method, of class WhitestarMediaComparator.
     */
    @Test
    public void testSimilarMusic2() {
        System.out.println("similar");
        WhitestarMusic predicate = new WhitestarMusic();
        WhitestarMusic musicTrack = new WhitestarMusic();
        predicate.setArtist("The Rolling Stones");
        musicTrack.setArtist("The Beatles");
        boolean expResult = false;
        boolean result = WhitestarMediaComparator.similar(predicate, musicTrack);
        assertEquals(expResult, result);
    }
    
       /**
     * Test of similarMusic method, of class WhitestarMediaComparator. This time with differing types
     */
    @Test
    public void testSimilarMusic3() {
        System.out.println("similar");
        boolean expResult = false;
        boolean result = WhitestarMediaComparator.similar(media1, media2);
        assertEquals(expResult, result);
    }
}
